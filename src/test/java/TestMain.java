import cn.hutool.core.collection.CollUtil;
import com.thirdGd.ThirdGdCommUtils;
import com.thirdGd.demo.MakeEnum;
import com.thirdGd.demo.SimpleFormInput;
import com.thirdGd.enums.SFSizeEnum;
import com.thirdGd.po.SFObject;

import java.lang.reflect.Field;

public class TestMain {
    public static void main(String[] args) {

        // 定义一个SF对象
        SimpleFormInput a1 = new SimpleFormInput();
        a1.setAbleEnum(MakeEnum.makeEnumObjects());
        a1.setRadioEnum(MakeEnum.makeEnumObjects());
        SFObject sfObject = ThirdGdCommUtils.makeBaseSFObject(a1);

        Field[] fields = ThirdGdCommUtils.getField(a1.getClass());
        CollUtil.toList(fields).forEach((Field filed) -> {
            // 简单字符串
            ThirdGdCommUtils.addStringItem(sfObject, filed, a1);
            // 简单下拉框
            ThirdGdCommUtils.addSelectItem(sfObject, filed, a1);
            // 简单开关切换
            ThirdGdCommUtils.addBooleanItem(sfObject, filed, a1);
            // 简单的单选框
            ThirdGdCommUtils.addRadioItem(sfObject, filed, a1);
        });

        System.out.println(sfObject.toJson().toString());
        System.out.println(SFSizeEnum.LARGE.toString());
    }


}
