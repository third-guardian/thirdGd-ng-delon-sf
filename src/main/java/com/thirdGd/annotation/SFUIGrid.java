package com.thirdGd.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 表单全局UI栅格
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface SFUIGrid {
    int span() default 24;

    int gutter() default 8;

    int arraySpan() default 0;

    int offset() default 0;

    boolean isArraySpan() default false;

    boolean isOffset() default false;
}
