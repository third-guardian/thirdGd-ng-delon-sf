package com.thirdGd.annotation;

import com.thirdGd.enums.SFSizeEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface SFString {

    /**
     * 标题
     * @return
     */
    String title();

    /**
     * 单位
     * @return
     */
    String optional() default "";


    /**
     * placeholder
     * @return
     */
    String placeholder() default "";

    /**
     * 长度
     * @return
     */
    int span() default 24;

    /**
     * 最大长度
     * @return
     */
    int maxLength() default 255;

    int minLength() default 0;

    /**
     * 尺寸
     * @return
     */
    SFSizeEnum size() default SFSizeEnum.DEFAULT;


}
