package com.thirdGd.annotation;

import com.thirdGd.enums.SFSizeEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 开关
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface SFBoolean {

    /**
     * 标题
     * @return
     */
    String title();

    /**
     * 单位
     * @return
     */
    String optional() default "";

    /**
     * 单位
     * @return
     */
    int span() default 24;

    /**
     * 选中时的内容
     * @return
     */
    String checkedChildren() default "是";

    /**
     * 	非选中时的内容
     * @return
     */
    String unCheckedChildren() default "否";

    String type() default "string";

    /**
     * 尺寸
     * @return
     */
    SFSizeEnum size() default SFSizeEnum.DEFAULT;
}
