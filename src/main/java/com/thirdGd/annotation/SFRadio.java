package com.thirdGd.annotation;

import com.thirdGd.enums.SFRadioButtonStyleEnum;
import com.thirdGd.enums.SFRadioStyleTypeEnum;
import com.thirdGd.enums.SFSizeEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 单选框
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface SFRadio {

    /**
     * 标题
     * @return
     */
    String title();

    /**
     * 单位
     * @return
     */
    String optional() default "";

    /**
     * 单位
     * @return
     */
    int span() default 24;

    String type() default "string";


    /**
     * 尺寸
     * @return
     */
    SFSizeEnum size() default SFSizeEnum.DEFAULT;

    /**
     * radio样式
     * @return
     */
    SFRadioStyleTypeEnum styleType() default SFRadioStyleTypeEnum.DEFAULT;

    /**
     * RadioButton 的风格样式，目前有描边和填色两种风格
     * @return
     */
    SFRadioButtonStyleEnum buttonStyle() default SFRadioButtonStyleEnum.OUTLINE;
}
