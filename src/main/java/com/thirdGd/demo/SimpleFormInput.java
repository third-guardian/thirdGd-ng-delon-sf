package com.thirdGd.demo;

import com.thirdGd.annotation.*;
import com.thirdGd.po.SFSchemaEnumObject;

import java.util.List;

@SFUI()
@SFUIGrid()
@SFRequired(keys = "name,status,")
@SFOrder(order = "able2,name")
public class SimpleFormInput {

    @SFEnum(name = "able")
    List<SFSchemaEnumObject> ableEnum;

    @SFEnum(name = "radio")
    List<SFSchemaEnumObject> radioEnum;


    @SFString(title = "name2", placeholder = "123123", optional = "123123")
    String name = "abc";

    @SFSelect(title = "able", placeholder = "able", optional = "able")
    String able;

    @SFSelect(title = "able2", placeholder = "able")
    String able2;

    @SFBoolean(title = "able2", optional = "able")
    boolean status;

    @SFRadio(title = "radio", optional = "radio")
    String radio;

    public List<SFSchemaEnumObject> getRadioEnum() {
        return radioEnum;
    }

    public void setRadioEnum(List<SFSchemaEnumObject> radioEnum) {
        this.radioEnum = radioEnum;
    }

    public String getRadio() {
        return radio;
    }

    public void setRadio(String radio) {
        this.radio = radio;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getAble2() {
        return able2;
    }

    public void setAble2(String able2) {
        this.able2 = able2;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAble() {
        return able;
    }

    public void setAble(String able) {
        this.able = able;
    }

    public List<SFSchemaEnumObject> getAbleEnum() {
        return ableEnum;
    }

    public void setAbleEnum(List<SFSchemaEnumObject> ableEnum) {
        this.ableEnum = ableEnum;
    }
}
