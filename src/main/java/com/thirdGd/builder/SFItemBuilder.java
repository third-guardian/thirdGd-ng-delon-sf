package com.thirdGd.builder;

import cn.hutool.core.util.StrUtil;
import com.thirdGd.annotation.SFBoolean;
import com.thirdGd.annotation.SFRadio;
import com.thirdGd.annotation.SFSelect;
import com.thirdGd.annotation.SFString;
import com.thirdGd.enums.SFRadioButtonStyleEnum;
import com.thirdGd.enums.SFRadioStyleTypeEnum;
import com.thirdGd.po.SFItemObject;
import com.thirdGd.po.SFItemUIObject;
import com.thirdGd.po.SFSchemaEnumObject;
import com.thirdGd.po.SFUIGridObject;

import java.util.List;

/**
 * 生成一个文本输入框
 */
public class SFItemBuilder {

    /**
     * 生成通用提示信息等UI配置
     *
     * @param optional
     * @param span
     * @return
     */
    public static void makeCommUI(SFItemUIObject ui, String optional,
                                  Integer span) {
        if (!StrUtil.hasEmpty(optional)) {
            ui.setOptional("(" + optional + ")");
        }
        if (span != null) {
            ui.setGrid(new SFUIGridObject(span));
        }
    }

    /**
     * 生成通用提示信息等UI配置
     *
     * @param optional
     * @param span
     * @param placeholder
     * @return
     */
    public static void makeCommUIObj(SFItemUIObject ui, String optional,
                                     Integer span,
                                     String placeholder
    ) {
        makeCommUI(ui, optional, span);
        if (!StrUtil.hasEmpty(placeholder)) {
            ui.setPlaceholder(placeholder);
        }
    }

    public static void makeRadioUIObj(SFItemUIObject ui, String optional,
                                      Integer span,
                                      SFRadioStyleTypeEnum styleType,
                                      SFRadioButtonStyleEnum buttonStyle
                                      ){
        makeCommUI(ui, optional, span);
        ui.setStyleType(styleType.toString());
        ui.setButtonStyle(buttonStyle.toString());
    }

    /**
     * 生成boolean提示信息等UI配置
     *
     * @param optional
     * @param span
     * @param checkedChildren
     * @param unCheckedChildren
     * @return
     */
    public static void makeBooleanUIObj(SFItemUIObject ui, String optional,
                                        Integer span,
                                        String checkedChildren,
                                        String unCheckedChildren
    ) {
        makeCommUI(ui, optional, span);
        ui.setCheckedChildren(checkedChildren);
        ui.setUnCheckedChildren(unCheckedChildren);
    }


    /**
     * 生成String的Input
     *
     * @param sfString SFString
     * @param xDefault Object
     * @return
     */
    public static SFItemObject buildInput(
            SFString sfString,
            Object xDefault
    ) {
        SFItemObject sfItemObject = new SFItemObject();
        sfItemObject.setType("string");
        sfItemObject.setTitle(sfString.title());
        SFItemUIObject ui = new SFItemUIObject();
        makeCommUIObj(ui, sfString.optional(), sfString.span(), sfString.placeholder());
        sfItemObject.setUi(ui);
        if (xDefault != null) {
            sfItemObject.setxDefault(xDefault);
        }
        return sfItemObject;
    }

    /**
     * 生成开关
     *
     * @param sfBoolean
     * @param xDefault
     * @return
     */
    public static SFItemObject buildBoolean(
            SFBoolean sfBoolean,
            Object xDefault
    ) {
        SFItemObject sfItemObject = new SFItemObject();
        sfItemObject.setType(sfBoolean.type());
        sfItemObject.setTitle(sfBoolean.title());
        SFItemUIObject ui = new SFItemUIObject();
        makeBooleanUIObj(ui,
                sfBoolean.optional(),
                sfBoolean.span(),
                sfBoolean.checkedChildren(),
                sfBoolean.unCheckedChildren()
        );
        ui.setWidget("boolean");
        sfItemObject.setUi(ui);


        if (xDefault != null) {
            sfItemObject.setxDefault(xDefault);
        }
        return sfItemObject;
    }


    /**
     * 生成Select
     *
     * @param sfSelect SFSelect
     * @param xEnum    List<SFSchemaEnumObject>
     * @param xDefault Object
     * @return
     */
    public static SFItemObject buildSelect(
            SFSelect sfSelect,
            List<SFSchemaEnumObject> xEnum,
            Object xDefault
    ) {
        SFItemObject sfItemObject = new SFItemObject();
        sfItemObject.setType(sfSelect.type());
        sfItemObject.setTitle(sfSelect.title());
        SFItemUIObject ui = new SFItemUIObject();
        makeCommUIObj(ui, sfSelect.optional(), sfSelect.span(), sfSelect.placeholder());
        ui.setWidget("select");
        sfItemObject.setUi(ui);
        sfItemObject.setxEnum(xEnum);

        if (xDefault != null) {
            sfItemObject.setxDefault(xDefault);
        }
        return sfItemObject;
    }

    /**
     * 生成Select
     *
     * @param sfRadio SFRadio
     * @param xEnum    List<SFSchemaEnumObject>
     * @param xDefault Object
     * @return
     */
    public static SFItemObject buildRadio(
            SFRadio sfRadio,
            List<SFSchemaEnumObject> xEnum,
            Object xDefault
    ) {
        SFItemObject sfItemObject = new SFItemObject();
        sfItemObject.setType(sfRadio.type());
        sfItemObject.setTitle(sfRadio.title());
        SFItemUIObject ui = new SFItemUIObject();
        makeRadioUIObj(ui, sfRadio.optional(), sfRadio.span(), sfRadio.styleType(), sfRadio.buttonStyle());
        ui.setWidget("radio");
        sfItemObject.setUi(ui);
        sfItemObject.setxEnum(xEnum);

        if (xDefault != null) {
            sfItemObject.setxDefault(xDefault);
        }
        return sfItemObject;
    }
}
