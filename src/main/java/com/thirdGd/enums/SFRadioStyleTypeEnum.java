package com.thirdGd.enums;

public enum SFRadioStyleTypeEnum {
    DEFAULT(""),
    BUTTON("button");
    // 定义一个 private 修饰的实例变量
    private String styleType;

    // 定义一个带参数的构造器，枚举类的构造器只能使用 private 修饰
    private SFRadioStyleTypeEnum(String styleType) {
        this.styleType = styleType;
    }


    // 重写 toString() 方法
    @Override
    public String toString() {
        return styleType;
    }

    public String getStyleType() {
        return styleType;
    }

    public void setStyleType(String styleType) {
        this.styleType = styleType;
    }
}
