package com.thirdGd.enums;

public enum SFSizeEnum {

    SMALL("small"),
    DEFAULT(""),
    LARGE("large");

    // 定义一个 private 修饰的实例变量
    private String size;

    // 定义一个带参数的构造器，枚举类的构造器只能使用 private 修饰
    private SFSizeEnum(String size) {
        this.size = size;
    }

    // 定义 get set 方法
    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    // 重写 toString() 方法
    @Override
    public String toString(){
        return size;
    }

}
