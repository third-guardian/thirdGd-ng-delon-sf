package com.thirdGd.enums;

public enum SFRadioButtonStyleEnum {

    OUTLINE("outline"),

    SOLID("solid");

    // 定义一个 private 修饰的实例变量
    private String buttonStyle;

    // 定义一个带参数的构造器，枚举类的构造器只能使用 private 修饰
    private SFRadioButtonStyleEnum(String buttonStyle) {
        this.buttonStyle = buttonStyle;
    }


    // 重写 toString() 方法
    @Override
    public String toString() {
        return buttonStyle;
    }

    public String getButtonStyle() {
        return buttonStyle;
    }

    public void setButtonStyle(String buttonStyle) {
        this.buttonStyle = buttonStyle;
    }
}
