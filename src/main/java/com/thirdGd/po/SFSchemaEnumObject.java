package com.thirdGd.po;

import java.util.List;

public class SFSchemaEnumObject {

    public SFSchemaEnumObject() {
    }

    public SFSchemaEnumObject(String label, Object value) {
        this.label = label;
        this.value = value;
    }

    /**
     * 是否禁用状态
     */
    boolean disabled;
    /**
     * 文本
     */
    String label;
    /**
     * 文本
     */
    Object title;
    /**
     * 值
     */
    Object value;
    /**
     * 主键，适用部分小部件数据键名，例如：`tree-select`
     */
    String key;
    /**
     * 是否选中
     */
    boolean checked;
    /**
     * 组名，适用部分允许组列表的小部件，例如：`select`
     * - 组对应的文本为 `label`
     * - `children` 为子项
     */
    boolean group;

    boolean isLeaf;

    List<SFSchemaEnumObject> children;

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Object getTitle() {
        return title;
    }

    public void setTitle(Object title) {
        this.title = title;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public boolean isGroup() {
        return group;
    }

    public void setGroup(boolean group) {
        this.group = group;
    }

    public boolean isLeaf() {
        return isLeaf;
    }

    public void setLeaf(boolean leaf) {
        isLeaf = leaf;
    }

    public List<SFSchemaEnumObject> getChildren() {
        return children;
    }

    public void setChildren(List<SFSchemaEnumObject> children) {
        this.children = children;
    }
}


