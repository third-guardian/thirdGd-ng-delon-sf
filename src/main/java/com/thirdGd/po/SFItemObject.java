package com.thirdGd.po;

import java.math.BigDecimal;
import java.util.List;

/**
 * 表单项的具体类容
 */
public class SFItemObject {

    /**
     * 数据类型，支持 JavaScript 基础类型；注意项：
     *
     * - `integer` 表示整型，`number` 表示浮点型
     * - JSON 中 `date` 等同 `string` 类型
     * - 指定 `format` 标准参数可以自动适配渲染小部件
     * - 指定 `widget` 参数强制渲染小部件
     */
    String type;

    /**
     * 枚举，静态数据源，例如：`radio`、`checkbox` 等
     *
     * - `disabled` 属性表示：禁用状态
     * - `label` 属性表示：文本
     * - `value` 属性表示：返回值
     * - 基础数据类型数组会自动转化成 `SFSchemaEnum` 数组格式
     */
    List<SFSchemaEnumObject> xEnum;

    /**
     * 最小值
     */
    BigDecimal minimum;

    /**
     * 最大值
     */
    BigDecimal maximum;

    /**
     * 倍数
     */
    Integer multipleOf;
    /**
     * 定义字符串的最大长度
     */
    Integer maxLength;
    /**
     * 定义字符串的最小长度
     */
    Integer minLength;

    /**
     * 验证输入字段正则表达式字符串，若指定 `format: 'regex'` 时务必指定
     */
    String pattern;

    /**
     * 数据格式，[文档](http://json-schema.org/latest/json-schema-validation.html#rfc.section.7.3)
     * - `date-time` 日期时间，渲染为 `date`，[RFC3339](https://tools.ietf.org/html/rfc3339#section-5.6)
     * - `date`、`full-date` 日期，渲染为 `date`
     * - `time`、`full-time` 时间，渲染为 `time`
     * - `email` Email格式，渲染为 `autocomplete`
     * - 非标准：`week`，渲染为 `nz-week-picker`
     * - 非标准：`month`，渲染为 `nz-month-picker`
     * - `ip` IP地址，渲染为 `input`
     * - `uri` URL地址，渲染为 `upload`
     * - `regex` 正则表达式，必须指定 `pattern` 属性，渲染为 `input`
     * - `mobile` 手机号
     * - `id-card` 身份证
     * - `color` 颜色值
     */
    String format;
    /**
     * 属性描述，相当于 `label` 值，按以下规则展示：
     * - 当值为 `null`、`undefined` 时使用 `key` 替代
     * - 当值为 `''` 空字符串表示不展示 `label` 部分，例如：`checkbox` 可能需要
     */
    String title;

    /**
     * 属性目的性解释
     */
    String description;
    /**
     * 默认值
     */
    Object xDefault; // default

    boolean readOnly;
    /**
     * **唯一非标准：
     * ** 定UI配置信息，优先级高于 `sf` 组件 `ui` 属性值
     */
    SFItemUIObject ui;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<SFSchemaEnumObject> getxEnum() {
        return xEnum;
    }

    public void setxEnum(List<SFSchemaEnumObject> xEnum) {
        this.xEnum = xEnum;
    }

    public BigDecimal getMinimum() {
        return minimum;
    }

    public void setMinimum(BigDecimal minimum) {
        this.minimum = minimum;
    }

    public BigDecimal getMaximum() {
        return maximum;
    }

    public void setMaximum(BigDecimal maximum) {
        this.maximum = maximum;
    }

    public Integer getMultipleOf() {
        return multipleOf;
    }

    public void setMultipleOf(Integer multipleOf) {
        this.multipleOf = multipleOf;
    }

    public Integer getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(Integer maxLength) {
        this.maxLength = maxLength;
    }

    public Integer getMinLength() {
        return minLength;
    }

    public void setMinLength(Integer minLength) {
        this.minLength = minLength;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Object getxDefault() {
        return xDefault;
    }

    public void setxDefault(Object xDefault) {
        this.xDefault = xDefault;
    }

    public boolean isReadOnly() {
        return readOnly;
    }

    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    public SFItemUIObject getUi() {
        return ui;
    }

    public void setUi(SFItemUIObject ui) {
        this.ui = ui;
    }
}
