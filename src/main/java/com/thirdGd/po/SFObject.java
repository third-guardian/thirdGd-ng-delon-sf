package com.thirdGd.po;

import cn.hutool.json.JSONObject;
import com.thirdGd.ThirdGdCommUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 表单全部信息
 */
public class SFObject {


    /**
     * ui配置
     */
    SFUIObject ui;

    /**
     * 必填字段
     */
    List<String> required;


    /**
     * 表单属性
     */
    Map<String, SFItemObject> properties;

    public SFObject(){
        this.properties = new HashMap<>();
        this.required = new ArrayList<>();
    }


    /**
     * 将对象转为json对象
     *
     * @return JSONObject
     */
    public JSONObject toJson() {
        Map<String, Object> map = ThirdGdCommUtils.convertToMap(this);
        return ThirdGdCommUtils.convertToJson(map);
    }

    public SFUIObject getUi() {
        return ui;
    }

    public SFObject setUi(SFUIObject ui) {
        this.ui = ui;
        return this;
    }

    public List<String> getRequired() {
        return required;
    }

    public void setRequired(List<String> required) {
        this.required = required;
    }

    public Map<String, SFItemObject> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, SFItemObject> properties) {
        this.properties = properties;
    }

    /**
     * 添加属性
     *
     * @param propertyKey String
     * @param property    SFItemObject
     * @return SFObject
     */
    public void addProperty(String propertyKey, SFItemObject property) {
        if(required != null) properties.put(propertyKey, property);
    }

    /**
     * 删除属性
     *
     * @param propertyKey propertyKey
     * @return SFObject
     */
    public void removeProperty(String propertyKey) {
        if (required != null && properties.containsKey(propertyKey)) {
            properties.remove(propertyKey);
        }
    }

    /**
     * 添加必填字段
     *
     * @param propertyKey String
     * @return SFObject
     */
    public void addRequiredKey(String propertyKey) {
        if (required != null && !required.contains(propertyKey)) {
            required.add(propertyKey);
        }
    }

    /**
     * 删除必填字段
     *
     * @param propertyKey String
     * @return SFObject
     */
    public void removeRequiredKey(String propertyKey) {
        if (required != null && required.contains(propertyKey)) {
            required.remove(propertyKey);
        }
    }
}
