package com.thirdGd.po;

/**
 * 表单具体项的UI属性
 */
public class SFItemUIObject {
    /**
     * 是否隐藏
     */
    boolean hidden;
    /**
     * `label` 栅格占位格数，默认：`5`
     * - `0` 时相当于 `display: none`
     * - 限 `horizontal` 水平布局有效
     */
    Integer spanLabel;
    /**
     * `control` 栅格占位格数，默认：`19`
     * - `0` 时相当于 `display: none`
     * - 限 `horizontal` 水平布局有效
     */
    Integer spanControl;
    /**
     * `control` 栅格左侧的间隔格数，间隔内不可以有栅格
     * - 限 `horizontal` 水平布局有效
     */
    Integer offsetControl;
    /**
     * `label` 固定宽度
     * - 限 `horizontal` 水平布局有效
     */
    Integer spanLabelFixed;
    /**
     * 提示信息
     */
    String placeholder;
    /**
     * 标签可选信息
     */
    String optional;
    /**
     * 标签可选帮助，使用 `nz-tooltip` 展示
     */
    String optionalHelp;

    /**
     * 响应式属性
     */
    SFUIGridObject grid;

    /**
     * 指定采用什么小部件渲染，所有小部件名可[查阅文档](https://ng-alain.com/)
     */
    String widget;

    /**
     * 大小，等同 nzSize
     */
    String size;

    /**
     * 选中时的内容
     */
    String checkedChildren;

    /**
     * 非选中时的内容
     */
    String unCheckedChildren;

    /**
     * radio 的样式
     */
    String styleType;

    /**
     * RadioButton 的风格样式，目前有描边和填色两种风格
     */
    String buttonStyle;

    public String getStyleType() {
        return styleType;
    }

    public void setStyleType(String styleType) {
        this.styleType = styleType;
    }

    public String getButtonStyle() {
        return buttonStyle;
    }

    public void setButtonStyle(String buttonStyle) {
        this.buttonStyle = buttonStyle;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public Integer getSpanLabel() {
        return spanLabel;
    }

    public void setSpanLabel(Integer spanLabel) {
        this.spanLabel = spanLabel;
    }

    public Integer getSpanControl() {
        return spanControl;
    }

    public void setSpanControl(Integer spanControl) {
        this.spanControl = spanControl;
    }

    public Integer getOffsetControl() {
        return offsetControl;
    }

    public void setOffsetControl(Integer offsetControl) {
        this.offsetControl = offsetControl;
    }

    public Integer getSpanLabelFixed() {
        return spanLabelFixed;
    }

    public void setSpanLabelFixed(Integer spanLabelFixed) {
        this.spanLabelFixed = spanLabelFixed;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    public String getOptional() {
        return optional;
    }

    public void setOptional(String optional) {
        this.optional = optional;
    }

    public String getOptionalHelp() {
        return optionalHelp;
    }

    public void setOptionalHelp(String optionalHelp) {
        this.optionalHelp = optionalHelp;
    }

    public SFUIGridObject getGrid() {
        return grid;
    }

    public void setGrid(SFUIGridObject grid) {
        this.grid = grid;
    }

    public String getWidget() {
        return widget;
    }

    public void setWidget(String widget) {
        this.widget = widget;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getCheckedChildren() {
        return checkedChildren;
    }

    public void setCheckedChildren(String checkedChildren) {
        this.checkedChildren = checkedChildren;
    }

    public String getUnCheckedChildren() {
        return unCheckedChildren;
    }

    public void setUnCheckedChildren(String unCheckedChildren) {
        this.unCheckedChildren = unCheckedChildren;
    }
}
