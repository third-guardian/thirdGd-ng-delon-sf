package com.thirdGd.po;

/**
 * 表单的总体样式布局
 */
public class SFUIGridObject {

    /**
     * 栅格占位格数，为 `0` 时相当于 `display: none`
     */
    Integer span;
    /**
     * 栅格间隔
     */
    Integer gutter;

    /**
     * 数据栅格占位格数，为 `0` 时相当于 `display: none`
     */
    Integer arraySpan;

    /**
     * 栅格左侧的间隔格数，间隔内不可以有栅格
     */
    Integer offset;


    public SFUIGridObject(){
        this.span = 24;
        this.gutter = 8;
    }

    public SFUIGridObject(Integer span){
        this.span = span;
        this.gutter = 8;
    }

    public SFUIGridObject(Integer span, Integer gutter){
        this.span = span;
        this.gutter = gutter;
    }

    public Integer getSpan() {
        return span;
    }

    public void setSpan(Integer span) {
        this.span = span;
    }

    public Integer getGutter() {
        return gutter;
    }

    public void setGutter(Integer gutter) {
        this.gutter = gutter;
    }

    public Integer getArraySpan() {
        return arraySpan;
    }

    public void setArraySpan(Integer arraySpan) {
        this.arraySpan = arraySpan;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }
}
