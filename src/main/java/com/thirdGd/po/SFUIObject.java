package com.thirdGd.po;

import java.util.ArrayList;
import java.util.List;

/**
 * 表单主要样式设置
 */
public class SFUIObject {

    public SFUIObject(Integer spanLabel, Integer spanControl, SFUIGridObject grid){
        this.spanLabel = spanLabel;
        this.spanControl = spanControl;
        this.grid = grid;
        this.order = new ArrayList<>();
    }


    Integer spanLabel;

    Integer spanControl;

    SFUIGridObject grid;

    /**
     * 字段排序
     */
    List<String> order;

    public Integer getSpanLabel() {
        return spanLabel;
    }

    public void setSpanLabel(Integer spanLabel) {
        this.spanLabel = spanLabel;
    }

    public Integer getSpanControl() {
        return spanControl;
    }

    public void setSpanControl(Integer spanControl) {
        this.spanControl = spanControl;
    }

    public SFUIGridObject getGrid() {
        return grid;
    }

    public void setGrid(SFUIGridObject grid) {
        this.grid = grid;
    }

    public List<String> getOrder() {
        return order;
    }

    public void setOrder(List<String> order) {
        this.order = order;
    }


    /**
     * 添加顺序字段
     * @param key String
     */
    public void addOrderKey(String key) {
        if (this.order != null && !order.contains(key)) {
            this.order.add(key);
        }
    }

}
