# 说明

此项目是一个为[**ng-alain**](https://ng-alain.com)项目中的[**simple-form**](https://ng-alain.com/form/getting-started/zh)的**SFSchema**生成工具。

## 设计初衷

**SFSchema**的json结构数据由服务端生成，减少前端的代码量，提高前端代码业务的灵活性。

## 设计思路

为每一个有需要的form表单，设置一个**Object**，并通过其中的属性和自定义注解，实现对特定属性值的获取，然后按**SFSchema**的需求，生成json结构。

## 预期结果

实现**SFSchema**的json结构实现对枚举的赋值、对默认值的赋值。


# 目录结构
```
+ src
|---main                                     
|     +---java
|           +---com
|                +---thirdGd
|                       +---annotation  // 自定义注解
|                       +---builder     // SFSchema生成逻辑
|                       +---demo        // form的Object举例
|                       +---enums       // SFSchema的枚举
|                       +---po          // SFSchema的UIObject等
+---test
|     +---java
|           +---TestMain

```



# 使用举例

## form属性设置

参考`src/main/java/com/thirdGd/demo/SimpleFormInput`

## 实现流程

参考`src/test/java/TestMain`

